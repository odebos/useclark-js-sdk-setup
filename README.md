UseClark
=========

Simpel quick setup for the UseClark javascript SDK.

API key
-------
To retrieve an API Key please send an email to mark@useclark.com.

Setup example
-------
```
var USECLARK = USECLARK || {
  onReady: function() {
    var credentials = {
      clientId: "123456", // (optional) Only needed for modes "heatmap-reading" and "heatmap-sentence".
      apiSecret: "123456abcdefg123456ABCDEFG123456", // (optional) Only needed for modes "heatmap-reading" and "heatmap-sentence".
      apiKey: "123456abcdefg123456ABCDEFG123456" // Always needed
    };
    var mode = "reading"; // Can also be "heatmap-reading" or "heatmap-sentence"
    var element = "#article"; // (optional) Can be an id, class or jquery object. 
    USECLARK.init(credentials, mode, element);
  }
};

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "http://useclark.com/UseClark-0.1.0.min.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'useclark-js'));
```

Three modes
-------
- "reading" - Actual reading mode for users.
- "heatmap-reading" - Shows a layover heatmap of reading statistics.
- "heatmap-sentence" - Shows a layover heatmap of saved sentences statistics.

Support
-------
For bugs and issues please email oscar@useclark.com.